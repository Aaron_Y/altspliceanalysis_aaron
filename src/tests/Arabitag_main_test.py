"""Pilot script for unittesting ArabiTag software"""

import os, sys, unittest


#this will add AltSplicing and TopHatJunctions to the sys path so that files in their 
#folders can be imported as modules
#if the location of this file is changed, this section will have to be changed as well
current_dir = os.path.split(os.path.abspath(__file__))[0]
sys.path.append(os.path.join(current_dir, "..", "AltSplicing"))
sys.path.append(os.path.join(current_dir, "..", "TopHatJunctions"))



#test ArabiTagMain.main's abt output for correct alt splicing output
#can be run with empty .bed file for estsInFile
def test_ArabiTagMain_abt_file(genesInFile,estsInFile,suffix,outPath,flank):
	from ArabiTagMain import main
	main(genesInFile,estsInFile,suffix,outPath,flank)
	with open("{0}AltSplicing_{1}.abt".format(outPath,suffix), "r") as abt:
		return abt.read().strip()

#The followiung variables will test LOC_Os01g01720, which has an alternative accepter site
AS_bed_file = "LOC_Os01g01720.bed"
AS_ests_file = "allJunctions_test1.bed"
AS_suffix = "test1"
AS_flank = 5
AS_outpath = "LOC_Os01g01720"
#LOC_Os01g01720 may have 2 possible ways to display the data:
option1 = "LOC_Os01g01720	LOC_Os01g01720.2	LOC_Os01g01720.1	383861,88,-1	383046,818,-1	383864,85,-1	AS"
option2 = "LOC_Os01g01720	LOC_Os01g01720.1	LOC_Os01g01720.2	383864,85,-1	383046,818,-1	383861,88,-1	AS"


#the following variables will test retained_intron_test_LOC_Os01g01040.bed
LOC_Os01g01040_RI_bed_file = "retained_intron_LOC_Os01g01040.bed"
LOC_Os01g01040_RI_ests_file = "allJunctions_test1.bed"
LOC_Os01g01040_RI_suffix = "test1"
LOC_Os01g01040_RI_flank = 5
LOC_Os01g01040_RI_outpath = "retained_intron_LOC_Os01g1040"

#the following variables will test retained_intron.bed
RI_bed_file = "retained_intron.bed"
RI_ests_file = "allJunctions_test1.bed"
RI_suffix = "test1"
RI_flank = 5
RI_outpath = "retained_intron_LOC_Os01g1040"


class MyTest(unittest.TestCase):
	def test_LOC_Os1g01720(self):
		self.assertEqual(test_ArabiTagMain_abt_file(AS_bed_file, AS_ests_file, AS_suffix, AS_outpath, AS_flank), option1 or option2)
	def test_retained_intron_LOC_Os01g01040(self):
		self.assertEqual(test_ArabiTagMain_abt_file(LOC_Os01g01040_RI_bed_file, LOC_Os01g01040_RI_ests_file, LOC_Os01g01040_RI_suffix, LOC_Os01g01040_RI_outpath, LOC_Os01g01040_RI_flank), 'LOC_Os01g01040\tLOC_Os01g01040.2\tLOC_Os01g01040.3\t19629,101,1\t19530,793,1\tNA\tRI')
	def test_retained_intron(self):
		self.assertEqual(test_ArabiTagMain_abt_file(RI_bed_file, RI_ests_file, RI_suffix, RI_outpath, RI_flank), option1 or option2)


if __name__ == "__main__":
	unittest.main()
