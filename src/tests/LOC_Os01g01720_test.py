"""Pilot script for unittesting ArabiTag software"""

import os, sys, unittest


#this will add AltSplicing and TopHatJunctions to the sys path so that files in their 
#folders can be imported as modules
#if the location of this file is changed, this section will have to be changed as well
current_dir = os.path.split(os.path.abspath(__file__))[0]
sys.path.append(os.path.join(current_dir, "..", "AltSplicing"))
sys.path.append(os.path.join(current_dir, "..", "TopHatJunctions"))



#test ArabiTagMain.main's abt output for correct alt splicing output
#can be run with empty .bed file for estsInFile
def test_ArabiTagMain_abt_file(genesInFile,estsInFile,suffix,outPath,flank):
	from ArabiTagMain import main
	main(genesInFile,estsInFile,suffix,outPath,flank)
	with open("{0}AltSplicing_{1}.abt".format(outPath,suffix), "r") as abt:
		return abt.read().strip()

#The followiung variables will test LOC_Os01g01720
bed_file = "LOC_Os01g01720.bed"
ests_file = "allJunctions_test1.bed"
suffix = "test1"
flank = 5
outpath = "LOC_Os01g01720"

"""
the abt file should have following information in the header:
1: locus				ex: LOC_Os01g01720	
2: splice variant 1			ex: LOC_Os01g01720.2
3: splice variant 2			ex: LOC_Os01g01720.1
4a: start intron v1			ex: 383861
4b: v1 intron size			ex: 88
4c: direction of read			ex: -1
5a: location of exon (including UTR)	ex: 383046
5b: overlapping area			ex: 818
5c: direction of read			ex: -1
6a: start of v2 intron read		ex: 383864
6b: v2 intron size			ex: 85
6c: direction of read			ex: -1
7:  class of alt splice event		ex: AS

With the following header layout:
1	2	3 4a,4b,4c	5a,5b,5c	6a,6b,6c	7

"""

class MyTest(unittest.TestCase):
	def test_LOC_Os1g01720(self):
	#LOC_Os01g01720 may have 2 possible ways to display the data:
		option1 = "LOC_Os01g01720	LOC_Os01g01720.2	LOC_Os01g01720.1	383861,88,-1	383046,818,-1	383864,85,-1	AS"
		option2 = "LOC_Os01g01720	LOC_Os01g01720.1	LOC_Os01g01720.2	383864,85,-1	383046,818,-1	383861,88,-1	AS"
		self.assertEqual(test_ArabiTagMain_abt_file(bed_file, ests_file, suffix, outpath, flank), option1 or option2)

if __name__ == "__main__":
	unittest.main()
