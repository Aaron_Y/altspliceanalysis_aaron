#! /usr/bin/python

__author__="auser"
__date__ ="$Jun 17, 2009 11:49:36 AM$"

if __name__ == "__main__":
    print "Hello World";


class AltSpliceModel:

    def __init__(self,gene1,gene2):
        self.setGene1(gene1)
        self.setGene2(gene2)
        self.setMappings()
        """print gene1[0].getDisplayId()
        print self.map1
        print gene2[0].getDisplayId()
        print self.map2"""

    def setGene1(self,gene):
        self.gene1 = gene
    def getGene1(self):
        return self.gene1

    def setGene2(self,gene):
        self.gene2 = gene
    def getGene2(self):
        return self.gene2

    def setMappings(self):
        """
        This will number our gene models and give us their coordinates.
        I chose to extract this information once, do the operations and
        then extract
        """
        self.map1 = []
        for feature in self.getGene1():
            type = feature.getType()
            if type == 'exon':
                self.map1.append([1,feature.getStart(),feature.getStart()+feature.getLength()])
            elif type == 'intron':
                self.map1.append([2,feature.getStart(),feature.getStart()+feature.getLength()])

        self.map2 = []
        for feature in self.getGene2():
            type = feature.getType()
            if type == 'exon':
                self.map2.append([4,feature.getStart(),feature.getStart()+feature.getLength()])
            elif type == 'intron':
                self.map2.append([8,feature.getStart(),feature.getStart()+feature.getLength()])

    def extractAS_Events(self):
        """
        This will create the AS model. See the lab notebook for specifics

        score = gene1-gene2 overlap
        1  = exon-none overlap #shouldn't be possible!?
        2  = intron-none overlap #shouldn't be possible!?
        4  = none-exon overlap #shouldn't be possible!?
        5  = exon-exon overlap
        6  = intron-exon overlap*
        8  = none-intron overlap #shouldn't be possible!?
        9  = exon-intron overlap*
        10 = intron-intron overlap 
        """

        events = []
        
        count1 = len(self.getGene1())
        count2 = len(self.getGene2())
        i = 0; j = 0

        while (i < count1) and (j < count2):
            score = self.map1[i][0] + self.map2[j][0]
            end = min(self.map1[i][2], self.map2[j][2])
            
            if score == 6:#geneAbsent = map1
                e = self.splicingCheck(self.getGene1(),i,self.getGene2(),j)
                if e:
                    events.extend(e)
                
            elif score == 9:#geneAbsent = map2
                e = self.splicingCheck(self.getGene2(),j,self.getGene1(),i)
                if e:
                    events.extend(e)

            #if we're at the end of a feature, move on
            if end == self.map1[i][2]:
                i +=1
            if end == self.map2[j][2]:
                j += 1

        return events

    def splicingCheck(self,geneAbsent,i,genePresent,j):
        """
        We have a splicing event, so lets figure out what type
        and which features we need to extract
        """
        featA = [geneAbsent[i-1],geneAbsent[i],geneAbsent[i+1]]

        #Try to look at all possible splice types with
        #our geneAbsent. if we get an error, that's okay
        #it is possible that we're looking for something that isn't possible.
        #we don't want that returned, so we ignore the error.
        ret = []

        if genePresent[j].contains(geneAbsent[i]):
            type = 'RI'#Retained Intron
            featP = [genePresent[j]]
            ret.append(AS_EventFeatures(featA,featP,type))

        try:
            #because of Exon skipping. We look at both of these.
            if geneAbsent[i].overlaps(genePresent[j+1]):
                type = 'DS'#Donator Site
                featP = [genePresent[j],genePresent[j+1],genePresent[j+2]]
                ret.append(AS_EventFeatures(featA,featP,type))
        except IndexError:
            pass;#I look for it, but it might not be there
        try:
            if geneAbsent[i].overlaps(genePresent[j-1]):
                type = 'AS'#Acceptor Site
                featP = [genePresent[j-2],genePresent[j-1],genePresent[j]]
                ret.append(AS_EventFeatures(featA,featP,type))
        except IndexError:
            pass;

        """
        because of our truncation style and our mapping scheme
        sometimes our map is looking at an intron and an exon at the same time
        but they do not actually overlap.
        
        for example
        A)    XXX-----XXXX...
        B) XXX--------XXXX...
        lets say that A doesn't start early enough to meet the critical start
        criteria. During Truncation, B loses its first exon and first intron.
        A)    XXX-----XXXX...
        B)            XXXX...
        Now, when we  look for splice sites, the algorithm compares A's first exon
        and then A's first intron with B's "first" exon. when the intron and exon
        are being compared, we look for a splicing event in this method without
        success, and for good reason.
        """
        return ret

class NoSplicingFoundException(Exception):
       def __init__(self, value):
           self.parameter = value
       def __str__(self):
           return repr(self.parameter)

class AS_EventFeatures:

    def __init__(self,geneAbsent,genePresent,type):
        """
        This is a temporary place holder for our AS event features.
        """
        self.setGeneAbsent(geneAbsent)
        self.setGenePresent(genePresent)
        self.setType(type)

    def setGeneAbsent(self,a):
        self.geneAbsent = a
    def getGeneAbsent(self):
        return self.geneAbsent

    def setGenePresent(self,p):
        self.genePresent = p
    def getGenePresent(self):
        return self.genePresent

    def setType(self,t):
        if type == None:
            raise Exception()
        self.type = t
    def getType(self):
        return self.type

    def __str__(self):
        return self.getGeneAbsent()[0].getDisplayId()+","+self.getGenePresent()[0].getDisplayId()+","+self.getType()



