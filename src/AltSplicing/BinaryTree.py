#! /usr/bin/python
import sys
__author__="auser"
__date__ ="$Jun 15, 2009 6:15:45 PM$"

if __name__ == "__main__":
    print "Hello World";


class RangeTree:
    """
    A binary tree that works with objects that have
    getStart() getLength() getName() methods
    """
    def __init__(self):
        self.root = None

    def add(self,iRange):
        if self.root == None:
            self.root = Node(iRange)
        else:
            self.root.add(iRange)
        #print iRange.getStart(),iRange.getEnd(); raw_input()

    def map(self,rangeObject):
        """Finds the node this range maps onto and returns the names inside of it"""
        results = self.root.map(rangeObject)
        if results:
            ret = []
            for x in results:
                ret.append(x.getName())
            return ret
        else:
            return False

class Node:
    """
    Binary Tree Node
    """

    def __init__(self,iRange):
        """
        Range is from ArabiTools IdentifiableRange
        """
        self.iRanges = [iRange]
        self.setNodeMin(iRange.getStart())
        self.setNodeMax(iRange.getEnd())
        self.leftChild = None
        self.rightChild = None

    def add(self,iRange):
        if iRange.getEnd() < self.getNodeMin():
            #sys.stdout.write("Left,")
            self.setLeftChild(iRange)
        elif iRange.getStart() > self.getNodeMax():
            #sys.stdout.write("Right,")
            self.setRightChild(iRange)
        else:
            #sys.stdout.write("Overlap")
            self.addRange(iRange)

    def map(self,rangeObject):
        """find the node this Range falls in or return false"""
        if (rangeObject.getStart() + rangeObject.getLength()) < self.getNodeMin():
            if self.getLeftChild() == None:
                return False
            else:
                return self.getLeftChild().map(rangeObject)
        elif rangeObject.getStart() > self.getNodeMax():
            if self.getRightChild() == None:
                return False
            else:
                return self.getRightChild().map(rangeObject)
        else:
            return self.getRanges()
        
    def setLeftChild(self,iRange):
        if self.getLeftChild() == None:
            self.leftChild = Node(iRange)
        else:
            self.getLeftChild().add(iRange)

    def getLeftChild(self):
        return self.leftChild

    def setRightChild(self,iRange):
        if self.getRightChild() == None:
            self.rightChild = Node(iRange)
        else:
            self.getRightChild().add(iRange)

    def getRightChild(self):
        return self.rightChild

    def addRange(self,iRange):
        curMin = iRange.getStart()
        curMax = iRange.getEnd()

        self.setNodeMin(min(self.getNodeMin(),curMin))
        self.setNodeMax(max(self.getNodeMax(),curMax))

        self.getRanges().append(iRange)

    def getRanges(self):
        return self.iRanges

    def setNodeMin(self,m):
        self.minimum = m

    def getNodeMin(self):
        return self.minimum

    def setNodeMax(self,m):
        self.maximum = m

    def getNodeMax(self):
        return self.maximum
