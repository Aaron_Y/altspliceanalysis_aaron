#! /usr/bin/python

__author__="Adam English"
__date__ ="$Jan 7, 2009 6:38:39 PM$"

"""
This helps make psl files and bed files from text representations instead of
manually counting each index cause that's rediculous.

Here is an example file

--- = intron
[X] = exon
e = EST
"""

txt = """
     e                          :EST1
       e                        :EST2
         e                      :EST3
            e                   :EST4
               e                :EST5
                   e            :EST6
                      e         :EST7
 eeee----------eeeee---eeeee    :EST-A
eeeee--------eeeeeee---eeeee    :EST-B
eeee-----eee---eee              :EST-C
[XXX]--------[XXXXX]---[XXX]    :FEATURE-1
[XXX]--------[XXX]-----[XXX]    :FEATURE-2
      [XXXXXX]---[XXX]          :G0
    [XXXXX]-----[XXXXXX]        :G1
    [XXX]-------[XXXXXX]        :G2
    [XXXXX]-----[XXXXXX]        :G3
    [XXXXX]-----[XX--XX]        :G4
[X]-[XXX]-------[XXXXXX]        :G5 *****ALT PROMOTER*****
    [XXX]-------[XXXXXX]-[X]    :G6 *******POLY A??******
"""

if __name__ == "__main__":
    txt = txt.split('\n')
    for x in txt:
        print "0         1         2         3 -TensBase\n0123456789012345678901234567890 -OnesBase"
        print x
        raw_input("--------------------------------")

