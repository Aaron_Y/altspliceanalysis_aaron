
"""
This file repaces and modifies bed_reader psl_reader
"""
__author__="ACEnglish"
__date__ ="$Jan 28, 2009 12:59:25 PM$"

import os,sys,string

import Utils.ssg_utils as utils
import Feature as feature

sep1 = '\t'
sep2 = ','

def readPsl(fname):
	"""
	Function: reads sequence features from psl format file
	Returns: A list of CompoundSeqFeature objects
	Args: fname - input file
	"""
	feats = []
	fh = utils.readfile(fname)
	for line in fh.readlines():
		feat = pslline2Feat(line)
		if feat:
			feats.append(feat)
	fh.close()
	
	return feats

def pslline2Feat(line):
    """
    Function: convert the given psl format line to a
              CompoundSeqFeature object
    Returns : a CompoundSeqFeature object
    Args    : line - the line
    """
    toks = line.split('\t')
    display_id=toks[9]

    seqname = toks[13]
    strand=toks[8]
    if strand == '+':
        strand = 1
    else:
        strand = -1
    tstarts = toks[20]
    tstarts = map(lambda x:int(x),tstarts.split(',')[0:-1])
    blockSizes = toks[18]
    blockSizes = map(lambda x:int(x),blockSizes.split(',')[0:-1])

    tstart = int(toks[15])
    tend = int(toks[16])
    alignlen = tend-tstart
    
    sortBlocks(tstarts, blockSizes)#ensure they're in order

    parent_feat = feature.CompoundSeqFeature(display_id=display_id,
                                             start=tstart,
                                             length=alignlen,
                                             seqname=seqname,
                                             strand=strand)
    nblocks = int(toks[17])
    i = 0
    while i < nblocks:
    	subfeat = feature.DNASeqFeature(display_id=display_id,
    									start=tstarts[i],
    									length=blockSizes[i],
    									strand=strand,
    									seqname=seqname,
    									type='exon')
    	
    	if i == 0:#first exon has a hard end.
    		subfeat.setSoftEnd(True)
    	
    	parent_feat.addFeat(subfeat)
    	
    	if i != nblocks-1:#Create intron if we didn't just create last exon
    		subfeat = feature.DNASeqFeature(display_id=display_id,
    										start=tstarts[i]+blockSizes[i],
    										length=tstarts[i+1]-tstarts[i]-blockSizes[i],
    										strand=strand,
    										seqname=seqname,
    										type='intron')
    		parent_feat.addFeat(subfeat)

    	else:#last exon has a hard end
    		subfeat.setSoftEnd(True)
    	
    	i = i + 1
    
    return parent_feat
	
def readBed(fname=None):
	"""
	Function: reads features from bed format file
	Returns: a list of CompoundSeqFeature objects
	Args: fname - name of file to read
	"""
	fh = utils.readfile(fname)
	feats = []
	failed_count = 0
	for line in fh.readlines():
		feat = bedline2feat(line)
		if feat:
			feats.append(feat)
		else:
			sys.stderr.write("Warning: Could not build feature from: %s"%line)
			failed_count=failed_count+1
	fh.close()
	sys.stderr.write("Failed to build features for %i models.\n"%failed_count)
	return feats

def bedline2feat(line):
    """
    Function: create CompoundSeqFeature object from BED format line
    Returns : a CompoundSeqFeature object
    Args    : line - BED format line (BED12 or BED-detail allowed)

    CompoundSeqFeature objects include exon and intron sub-features.

    Sometimes gene models are pathological - no known strand, overlapping
    exons. Return None when given pathological data.
    """
    toks = line.strip().split(sep1)
    seqname=toks[0]
    start = string.atoi(toks[1])
    end = string.atoi(toks[2])
    name = toks[3]
    strand = toks[5]
    blockcount=string.atoi(toks[9])
    blocklens=toks[10]
    blockstarts=toks[11]

    if strand == '+':
        strand = 1
    elif strand == '-':
        strand = -1
    elif strand == '.':
        sys.stderr.write("strand: . for %s\n"%name)
	return None   
    parent_feat = feature.CompoundSeqFeature(display_id=name,start=start,
					     length=end-start,
					     seqname=seqname,
					     strand=strand)
    
    if blockstarts.endswith(','):
    	bl_starts = map(lambda x:string.atoi(x),blockstarts.split(sep2)[0:-1])
    else:
    	bl_starts = map(lambda x:string.atoi(x),blockstarts.split(sep2))
    bl_starts = map(lambda x:x+start,bl_starts)
    if blocklens.endswith(','):
    	bl_lens = map(lambda x:string.atoi(x),blocklens.split(sep2)[0:-1])
    else:
    	bl_lens = map(lambda x:string.atoi(x),blocklens.split(sep2))

    #ensure that blocks sorted first to last.
    sortBlocks(bl_starts,bl_lens)
    
    i = 0
    while i < blockcount:
        subfeat = feature.DNASeqFeature(start=bl_starts[i],
                                        length=bl_lens[i],
                                        strand=strand,
                                        seqname=seqname,
                                        display_id=name,
                                        type='exon')
        parent_feat.addFeat(subfeat)
        
        if i != blockcount-1: #Create intron if we didn't just create last exon
		start = subfeat.getStart()+subfeat.getLength()
		intron_length = bl_starts[i+1]-bl_starts[i]-bl_lens[i]
		if intron_length < 0: 
			# happens when gene models contain overlapping exons
			# ex) M_acuminata_DH_Pahang_Jan_2016.bed
			sys.stderr.write("Warning: %s intron after exon %i has length %i. %i-(%i+%i)\n"%(name,i,intron_length,bl_starts[i+1],bl_starts[i],bl_lens[i]))
			return None
		subfeat = feature.DNASeqFeature(start=start,
						length=intron_length,
						strand=strand, 
						seqname=seqname, 
						display_id = name, 
						type='intron')
        	parent_feat.addFeat(subfeat)
        i = i + 1
    return parent_feat

def sortBlocks(starts,lengths):
    """
    Function: ensures the block starts read from a bed file are in order with a simple bubble sort.
              This needs to be checked for order to make introns
    Args: starts - list of block starts
          lengths - list of block lengths (these need to be moved in order with starts
    Returns: None, all sorting is done in place
    """

    for i in range(0,len(starts)-1):
        swapMade = False#short circuiting. if no swaps, we can quit
        for j in range(0,len(starts)-1-i):
            if starts[j+1]<starts[j]:
                tempS = starts[j]
                tempL = lengths[j]
                starts[j] = starts[j+1]
                lengths[j] = lengths[j+1]
                starts[j+1] = tempS
                lengths[j+1] = tempL
                swapMade = True

        if not swapMade: break
