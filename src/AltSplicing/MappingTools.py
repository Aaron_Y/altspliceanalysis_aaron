#! /usr/bin/python

__author__="ACEnglish"
__date__ ="$Jun 15, 2009 1:07:58 PM$"

import ArabiTools as tool
from BinaryTree import RangeTree

"""
A collection of methods that when used correctly group all genes together by locus
and also maps ESTs to individual gene models or entire Alternatice Splicing Events
"""

def createESTtoLocusMap(ESTs,locusCoordinateHash):
    """"
    Function: using a set of ESTs and a locusCoordinateHash, we'll create a
        dictionary with the structure of chr:{locus:[ests that are contained in locus span]}
        See locusCoordinateHash if you want to trace what this information means
    Args: ESTs - a dictionary of chromosome separated lists of
        CompountESTSeqFeature objects
          locusCoordinateHash - dictionary with strucutre chr:{Locus:Range())}
    """
    ret = {}
    #ferr = open("NONMAPPINGESTS",'w')
    #fout = open('ESTtoLocusMap.abt','w')
    
    for chr in locusCoordinateHash.keys():
        myTree = RangeTree()
        chrRet ={}
        for locus in locusCoordinateHash[chr].keys():
            myTree.add(locusCoordinateHash[chr][locus])
        
        if not chr in ESTs.keys():
        	continue;
        for est in ESTs[chr]:
            estsLoci = myTree.map(est)
            if estsLoci:
                for curLocus in estsLoci:
                	try:
                		chrRet[curLocus].append(est)
                	except KeyError:
                		chrRet[curLocus] = [est]

        ret[chr] = chrRet


    #fout.close()
    #ferr.close()
    
    return ret
        

def createLocusHash(geneModelsDict):
    """
    Function: Sorts all gene models by their locus and returns a dictionary
        with chr:{Locus:[geneModel<Feature>, geneModel<Feature>]} Structure
    Args: geneModels: A dictionary of CompoundDNASeqFeatures with
        structure of chr:[list of all gene Models]
    """

    ret = {}#this will have a chromosome:locusHash Structure.

    for chr in geneModelsDict.keys():
        chrRet = {}# this is the locushash for the current chromosome

        for curGeneModel in  geneModelsDict[chr]:
            # grab the locus
            # curLocus = curGeneModel.getDisplayId().split('.')[0]
            toks = curGeneModel.getDisplayId().split('.')
            curLocus = '.'.join(toks[0:len(toks)-1])

            #add the gene model to the dictionary
            try:
                chrRet[curLocus].append(curGeneModel)
            except KeyError:
                chrRet[curLocus] = [curGeneModel]

        ret[chr] = chrRet

    return ret

def createLocusCoordinateHash(geneModelsDict):
    """
    Function: creates a coordinate Hash Table with the structure
        chr:{Locus:Range()} structure that will help with
        building est mapping hash later.
    Args: geneModelsDict- A dictionary of CompoundDNASeqFeatures with
        structure of chr:{locus:[genemodel, geneModel]}. This should be
        what is returned from createLocusHash
    """

    ret = {}#this will be chromosome:{locus:(start,stop)}

    for chr in geneModelsDict.keys():
        chrRet = {}#this will hold the locus-coordinate has for the current chromosome
        for locus in geneModelsDict[chr].keys():
            locusMin = 1000000000000#the current locus' min and max (span)
            locusMax = -1
            for curGeneModel in geneModelsDict[chr][locus]:
                #minimum and maximum of the locus
                locusMin = min(locusMin, curGeneModel.getStart())
                locusMax = max(locusMax, curGeneModel.getStart()+curGeneModel.getLength())
            
            chrRet[locus] = tool.IdentifiableRange(locusMin,locusMax,locus)
            

        ret[chr] = chrRet

    return ret

def mapESTsToFeatures(ESTs,features):
    """
    Function: Tags features with supporting ESTs
    Returns: Nothing
    Args:   ESTs - a list of CompoundESTSeqFEatures
            feature - a list of CompoundDNASeqFEature

    NOTE!!!: We assume the ESTs and features sent to this method are on the same
    chromosome.
    """

    for curEST in ESTs:#for each est
        for curFeature in features:#for each feature
            #if curFeature.getSeqname() == curEST.getSeqname():
            if curFeature.overlaps(curEST,ignoreStarts=True):#no overlap, no chance for support
                indvFeats = curFeature.getFeats()#get the individual exons/introns
                indvESTs = curEST.sortedFeats()
                for (prev, intron, next) in tool.featureIterator(indvESTs):#get a set of three, exon - intron - exon
                    for feat in indvFeats: #for every individual exon/intron

                        if feat.getType() == 'exon':#if we're looking at an exon
                                #For each SE that IS tagged RI, find all ESTs contain an
                                #inferred exon with boundaries over eoverlap.
                                if prev != None and feat.overlaps(prev):
                                    feat.addTag(prev)
                                elif next != None and feat.overlaps(next):
                                    feat.addTag(next)

                        else:#we're looking at an intron
                        #dentify all ESTs whose alignments contain a gap (inferred intron)
                        #identical to either the boundaries of ialt or ioverlap and
                        #where the flanking matched regions in the ESTs cover at least 20 bases
                        #on either side of the inferred intron.
                            if intron != None and feat.equals(intron, ignoreStrand = True):#if the feature intron and est intron are identical
                                if prev.getLength() >= 20 and next.getLength() >= 20:#and there are 20+ bases on flanking sides of est
                                    feat.addTag(intron)
