"""Methods for sorting the data before working on it to decrease computation time"""
import ArabiTools

__author__="ACEnglish"
__date__ ="$Feb 14, 2009 7:24:46 PM$"
import threading
import ArabiTag
import FileWriter
import time
import math

def sortByChromosome(models):
    """
    Function:   Sorts all the incoming gene model by their Chromosome
    Returns:    A dictionary: key - Chromosome, Value - List of models
    Args:       Models: A list of Compound___SeqFeatures
    """
    ret = {}
    for x in models:
        key = x.getSeqname()
        if ret.has_key(key):
            ret[key].append(x)
        else:
            ret.update({key: [x]})

    return ret

def divideByStrand(GeneModels):
    """
    Divides Gene Models by their strand + or -, because they can never be candidates for Alternative Splicing
    Args: GeneModels - a list of CompoundDNASeqFeatures
    Returns: A tuple. (+ strand Features, - strand Features)

    Note: Don't divide ESTs by strand, they're independent of strand.
    """
    Pstrand = []
    Mstrand = []

    for x in GeneModels:
        if x.getStrand() == '+':
            Pstrand.append(x)
        else:
            Mstrand.apend(x)
results = []

class ChromosomeThread(threading.Thread):
    def __init__(self,feats,ests,createPickle = False, screen = None,i = 3):
        threading.Thread.__init__(self)
        self.feats = feats
        self.ests = ests
        self.pickle = createPickle
        self.screen = screen
        self.line = i

    def run(self):
        self.screen.addLine(self.feats[0].getSeqname(),self.line)
        splice = ArabiTag.findAltSplicingTuples(self.feats, self.ests,self.screen,self.line)
        
        self.screen.overrideLine(self.line,"Saving %s..." % (self.feats[0].getSeqname()))

        if self.pickle:
            FileWriter.pickleData(self.feats[0].getSeqname()+"AltSplicing",splice)

        results.append(splice)
        self.screen.overrideLine(self.line,self.feats[0].getSeqname() + " is finished." + time.asctime())

class ChromosomeTree:
    """B+ tree that sorts features into bins by chromosome"""

    def __init__(self, chromosomeLength, critical = None, div = 0):
        """
        chromosomeLength = length of a chromosome
        critical = dividing line for binary tree
        """
        #position on chromosome This Node divides
        self.divisions = div
        self.length = chromosomeLength

        if critical == None:
            self.criticalPos = self.length/2
        else:
            self.criticalPos = critical
            
        if self.divisions == 12:
            self.bottom = True
            self.bin = []
        else:
            self.setLeftNode()
            self.setRightNode()
            self.bin = []
            self.bottom = False

    def getLeftNode(self):
        return self.leftNode

    def getRightNode(self):
        return self.rightNode

    def setLeftNode(self):
        self.leftNode = ChromosomeTree(self.criticalPos, self.criticalPos -((self.length - self.criticalPos)/2), self.divisions + 1)

    def setRightNode(self):
        self.rightNode = ChromosomeTree(self.length, self.length - ((self.length - self.criticalPos)/2), self.divisions + 1)

    def addObject(self,seqFeature):
        """as an object with a range to the Tree"""
        if self.isBottomNode():
            self.addToBin(seqFeature)
        else:
            #If entire feature is to the left, add it to the left node
            if (seqFeature.getStart() + seqFeature.getLength()) <= self.criticalPos:
                self.getLeftNode().addObject(seqFeature)
            elif seqFeature.getStart() >= self.criticalPos:
                self.getRightNode().addObject(seqFeature)
            else:
                #If seqFeature starts to the right of the divide
                # and ends to the left of the divide
                # put the seqFeature in this node's bin.
                self.addToBin(seqFeature)

    def addToBin(self, object):
        """Adds an object to this particular bin"""
        self.bin.append(object)

    def getBin(self):
        return [self.bin]
    
    def isBottomNode(self):
        return self.bottom
    
    def getCriticalPos(self):
        return self.criticalPos

    def returnBins(self):
        """Returns all bins within a Tree. Starting from this node and down"""
        if self.isBottomNode():
            return self.getBin()
        else:
            #ret = self.getLeftNode().returnBins()
            #ret.append(self.getRightNode().returnBins())
            return self.getLeftNode().returnBins() + self.getBin() + self.getRightNode().returnBins()
        
