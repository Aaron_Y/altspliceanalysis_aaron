
__author__="ACEnglish"
__date__ ="$Feb 9, 2009 6:01:32 PM$"
import os, pickle, Feature

def writeSplicingEvents(SE,fileName):
	"""
	Writes a splicing event with the following fields. Tab delimited:

	AGI Locus id
	Genome Model id for GA(intron)
	Genome Model id for GP(exon)
	ioverlap: start,length,strand
	eoverlap: start,length,strand
	ialt: start,length,strand or NA
	type: Type of AS event
	GAests: List of ests that support the Gene Absent
	GPests: List of ests that support the Gene Present
	
	SE = a list of splicingEvents
		
	Also, strand corrects.
	
		
	if spliceType == 'DS' and iStrand == "-1":
		spliceType = 'AS'
	elif spliceType == 'AS' and iStrand == "-1":
		spliceType = 'DS'
	"""
	fh = open(fileName, 'w')

	for event in SE:
		# LocusId = event.getExonOverlap().getDisplayId().split('.')[0]
		toks = event.getExonOverlap().getDisplayId().split('.')
		LocusId = '.'.join(toks[0:len(toks)-1])
		GAid = event.getIntronOverlap().getDisplayId()
		GPid = event.getExonOverlap().getDisplayId()
		ioverlap = str(event.getIntronOverlap().getStart()) + "," + str(event.getIntronOverlap().getLength()) + "," + str(event.getIntronOverlap().getStrand())
		eoverlap = str(event.getExonOverlap().getStart()) + "," + str(event.getExonOverlap().getLength()) + "," + str(event.getExonOverlap().getStrand())
		
		iStrand = str(event.getIntronOverlap().getStrand());
		
		type = event.getType()
			
		if type == 'DS' and iStrand == "-1":
			type = 'AS'
		elif type == 'AS' and iStrand == "-1":
			type = 'DS'
			
		if type == 'RI':
			ialt = 'NA'
		else:
			ialt = str(event.getIntronAlt().getStart()) + "," + str(event.getIntronAlt().getLength()) + "," + str(event.getIntronAlt().getStrand())

		GAests = ",".join(event.getGAbsentSupport())
		GPests = ",".join(event.getGPresentSupport())

		fh.write('\t'.join([LocusId, GAid, GPid, ioverlap, eoverlap, ialt, type, GAests, GPests])+os.linesep)

	fh.close()

def pickleData(fileName,data):
	"""Creates fileName.pickle"""
	fh = open(fileName+".pickle",'w',1)
	pickle.dump(data,fh, pickle.HIGHEST_PROTOCOL)
	fh.close()
