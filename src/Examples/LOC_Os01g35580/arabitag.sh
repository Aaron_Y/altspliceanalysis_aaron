#!/bin/bash

# gene model file
B=LOC_Os01g35580.bed

# prefix for output files
S=test1

# need at least this many bases on either side of a junction
F=5

# need at least this many bases overlapping an intron for calling
# a retained intron
RI=10

../../TopHatJunctions/PreParse.py -s _$S *FJ.bed
../../AltSplicing/ArabiTagMain.py -g $B -e allJunctions_$S.bed -s $S -f $F
O=AltSplicing_$S.$F.txt
../../TopHatJunctions/PostParse.py -b $B AltSplicing_$S.abt AltSplicing_$S.f$F.AS.txt
../../TopHatJunctions/Add_RiGp_Support.py -o AltSplicing_$S.f$F.ASwiRI.txt -i AltSplicing_$S.f$F.AS.txt -f $RI -d .
