/*
Editing this file does not edit the database (obviously)
More importantly, All the Db.scripts make some assumption about how the db is structured.
If you edit something here, you best go hunt for who all you've affected.
The good news is that you should only affect thigns in src/DatabaseDevelopment. Unless you really messed something up.
*/

DROP TABLE IF EXISTS News;
DROP TABLE IF EXISTS AS_ChoiceSupport;
DROP TABLE IF EXISTS LibAttrValue;
DROP TABLE IF EXISTS Attribute;
DROP TABLE IF EXISTS ESTSupportGene;
DROP TABLE IF EXISTS EST_Record;
DROP TABLE IF EXISTS AS_Support_Count;
DROP TABLE IF EXISTS AS_Event;
DROP TABLE IF EXISTS GeneModel;
DROP TABLE IF EXISTS Library;


CREATE TABLE Library(
    LibraryId   INT NOT NULL AUTO_INCREMENT,
    information TEXT,
    genbank Boolean,
    PRIMARY KEY (LibraryId)
)ENGINE=INNODB;

CREATE TABLE EST_Record(
	ESTId	INT NOT NULL AUTO_INCREMENT,
	estName	VARCHAR(30),
    LibraryId   INT,
    GenBank_gi  VARCHAR(20),
    GenBank_Acc VARCHAR(20),
    PRIMARY KEY(ESTId),
    FOREIGN KEY (LibraryId)
        REFERENCES Library(LibraryId) ON UPDATE CASCADE ON DELETE CASCADE,
    INDEX index1 (estName),
    INDEX index2 (GenBank_Acc)
)ENGINE=INNODB;

CREATE TABLE Attribute(
    AttributeId INT NOT NULL AUTO_INCREMENT,
    Label       VARCHAR(40),
    PRIMARY KEY (AttributeId)
)ENGINE=INNODB;

/*
The First, Mandatory Attributes
1 - Name
2 - Description
3 - dbEST_LibID
*/
INSERT INTO Attribute (Label) VALUE ("Name");
INSERT INTO Attribute (Label) VALUE ("Description");
INSERT INTO Attribute (Label) VALUE ("dbESTid");

CREATE TABLE LibAttrValue(
    LibraryId   INT,
    AttributeId INT,
    LAvalue       TEXT,
    FOREIGN KEY (LibraryId)
        REFERENCES Library(LibraryId) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (AttributeId)
        REFERENCES Attribute(AttributeId) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;

CREATE TABLE GeneModel(
	chromosome VARCHAR(4),
	chromStart INT,
	chromEnd	BIGINT,
	GeneId	VARCHAR(15),
	strand	VARCHAR(1),
	thickStart	BIGINT,
	thickEnd	BIGINT,
	blockCount	INT,
	blockSizes	MEDIUMTEXT,
	blockStarts	MEDIUMTEXT,
	PRIMARY KEY(GeneId)
)ENGINE=INNODB;

CREATE TABLE ESTSupportGene(
	GeneId		VARCHAR(15),
	ESTId		INT,
	FOREIGN KEY (GeneId)
		REFERENCES GeneModel(GeneId) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (ESTId)
		REFERENCES EST_Record(ESTId) ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;

CREATE TABLE AS_Event(
	AS_Id	INT NOT NULL AUTO_INCREMENT,
	geneAbsentId	VARCHAR(15),
	genePresentId	VARCHAR(15),
	diffRegionGenomicStart	INT,
	diffRegionGenomicEnd	INT,
	type	VARCHAR(5),	
	PValue	FLOAT,
	PRIMARY KEY (AS_Id),
	FOREIGN KEY (geneAbsentId) 
		REFERENCES GeneModel(GeneId) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (genePresentId) 
		REFERENCES GeneModel(GeneId) ON UPDATE CASCADE ON DELETE CASCADE,
	INDEX index1(geneAbsentId, genePresentId, diffRegionGenomicStart, diffRegionGenomicEnd)
)ENGINE=INNODB;

CREATE TABLE AS_ChoiceSupport(
	AS_Id	INT,
	GeneId	VARCHAR(15),
	ESTId	INT,
	LibraryId	INT,
	FOREIGN KEY (GeneId)
		REFERENCES GeneModel(GeneId) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (ESTId)
		REFERENCES EST_Record(ESTId) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (LibraryId)
		REFERENCES Library(LibraryId) ON UPDATE CASCADE ON DELETE SET NULL,
	FOREIGN KEY (AS_Id)
		REFERENCES AS_Event(AS_Id) ON UPDATE CASCADE ON DELETE CASCADE,
	INDEX index1 (AS_Id, GeneId),
	INDEX index2 (AS_Id, LibraryId)
)ENGINE=INNODB;

CREATE OR REPLACE VIEW LibOverview AS (
    SELECT l.LibraryId, v1.LAvalue as "dbESTid", v2.LAValue as "LibName", v3.LAValue as "Desc", l.genbank
    FROM ((Library l LEFT JOIN LibAttrValue v1 ON l.LibraryId = v1.LibraryId AND v1.AttributeId = 3) 
       LEFT JOIN LibAttrValue v2 ON l.LibraryId = v2.LibraryId AND (v2.AttributeId = 1 OR v2.AttributeId IS NULL))
       LEFT JOIN LibAttrValue v3 ON l.LibraryId = v3.LibraryId AND (v3.AttributeId = 2 OR v3.AttributeId IS NULL));

CREATE OR REPLACE VIEW LAViewer AS (
	SELECT Library.LibraryId, Attribute.Label, LibAttrValue.LAValue 
	FROM Library, Attribute, LibAttrValue 
	WHERE LibAttrValue.LibraryId = Library.LibraryId AND LibAttrValue.AttributeId = Attribute.AttributeId);
	
CREATE TABLE News(
	Title VARCHAR(100),
	Author	VARCHAR(30),
	_Date	DATE,
	Body	TEXT);
	
CREATE TABLE AS_Support_Count(AS_Id INT, gaCount INT, gpCount INT, PRIMARY KEY(AS_Id), FOREIGN KEY(AS_Id) REFERENCES AS_Event(AS_Id) ON UPDATE CASCADE ON DELETE CASCADE)ENGINE=INNODB