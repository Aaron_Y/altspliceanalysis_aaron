"""
Creates XML that has format
	<library name="required" desc="also required" dbESTid="Null">
		<attribute label="label">Value</attribute>
		<attribute label="label">Value</attribute>
		<attribute label="label">Value</attribute>
		<members>
			<est name="estName" />
			<est name="estName" />
			<est name="estName" />
			<est name="estName" />
		</members>
	</library>
"""
		
import time,re, readline, sys

def readfile(fn=None):
	"""
	Function: Open a file for reading. 
	Returns : a file object
	Args	: fn - the name of the file

	It can be a plain text file or a
	file compressed using gzip. If the latter,
	the file name must terminate in characters
	.gz
	"""
	import gzip					  
	if not fn:
		sys.stderr.write("Error: need an input file\n")
		return None
	if fn.endswith('.gz'):
		fh=gzip.GzipFile(fn)
	else:
		fh=open(fn,'r')
	return fh
	
def getLibraryInfo():
	"""
	Function: Interacts with user to retrieve library information
	"""
	print "Welcome to the library builder. Let's get started.\n"
	print "First I need some required elements for your library:"
	
	libName = raw_input("\tWhat is the library's name? ")
	print
	desc = raw_input("\tWhat is the library's description? ")
	print "\n"
	print "Now I'll need the library's attributes. Enter as many attributes as you'd like and press enter to quit."
	
	attrs = {}
	while True: 
		label = raw_input("Enter the attribute's label: ")
		
		if label == "":
			break;
		
		value = raw_input("\tEnter the attributes value: ")
		
		attrs[label] = value
			
	ests = 	raw_input("Enter the directory to the EST's Alignment File (.bed or .psl) that belong with this library: ")
	
	outfile = raw_input("One last thing, What what is the file's name? (I'll add .xml to whatever you enter.): ")
	outfile += ".xml"
	createLibrary(libName, desc, attrs, ests, outfile)

#gen bank is going to call this several times
def createLibrary(libName,desc, attrs, estPath, outfile):
	"""
	Function: Creates the library.xml
	Args: 
		libName - the library's name, 
		desc-the library's description, 
		estPath - directory to all the ests in the library 
		attrs-library attributes dictionary
		outFile - What we're writing
	Returns: nothing
	"""
	#Templates
	libHead = '<library name="%s" desc="%s" dbESTid="Null">\n'
	attributeLine = '\t<attribute label="%s">%s</attribute>\n'
	estLine = '\t\t<est name="%s" />\n'
	
	fout = open(outfile,'w')
	
	fout.write(libHead % (libName, desc))
	for key in attrs:
		fout.write(attributeLine % (key, attrs[key]) )
	
	if re.search("\.bed", estPath.split("/")[-1]):
		col = 3
	elif re.search("\.psl", estPath.split("/")[-1]):
		col = 9
	else:
		sys.stderr.write("Please ensure you file is in .bed or .psl format.\nExiting...\n")
		sys.exit(1)
		

	fout.write('\t<members>\n')

	ests = readfile(estPath)
		
	for line in ests:
		fout.write(estLine % line.split('\t')[col])
	
	fout.write('\t</members>\n')
	fout.write('</library>\n')
	
	fout.close()
	
if __name__ == '__main__':
	getLibraryInfo()
	print "Have a Great Day!"