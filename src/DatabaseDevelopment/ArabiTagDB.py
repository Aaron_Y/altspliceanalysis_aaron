#!/bin/python

from optparse import OptionParser
import AddLibrary_ESTsToDb
import MySQLdb
import time, sys, re, gzip, zipfile

def readfile(f):
    """
    Function: Open a file for reading.
    Returns : a file handle
    Args    : the name of a file

    The given file can be compressed (file extension .gz)
    or a regular non-compressed file.

    """
    reg = re.compile(r'\.gz$')
    reg2 = re.compile(r'\.zip$')
    if reg.search(f):
        z = gzip.GzipFile(f)
    elif zipfile.is_zipfile(f):
        return None
    else:
        z = open(f)
    return z


def CreateNewDatabase(options):
	"""
	This method creates CREATE OR REPLACE the entire database but does not fill it!
	"""
	host = options.host
	database = options.database
	username = options.username
	password = options.password
	
	db = MySQLdb.connect(host=host,user=username,passwd=password,db=database)
	db.autocommit(True)
	cursor = db.cursor()
	theQuery = open('CreateDatabase.sql','r').read()
	theQueries = theQuery.split(';')
	#raw_input(dir(db))
	for q in theQueries:
		cursor.execute(q)
		
	cursor.close()
	
	cursor = db.cursor()
	#Add all the gene models to the database
	fn = options.genes
	fh = readfile(fn)
	
	theQuery = "INSERT INTO GeneModel (chromosome, chromStart, chromEnd, GeneId, strand, thickStart, thickEnd, blockCount, blockSizes, blockStarts) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) "
	values = []#all the values we're going to insert into the GeneModel table
	for line in fh.readlines():
		(seqname, start, end, name, score, strand, thick_start, thick_end, na, blockcount, blocklens, blockstarts) = line.strip().split("\t")
		values.append((seqname, start, end, name, strand, thick_start, thick_end, blockcount, blocklens, blockstarts))
	
	cursor.executemany(theQuery, values)
	
	#close connection to database
	cursor.close()
	db.close()
	
def AddToDatabase(options):
	"""
	Given a .ABT file from AltSplicing/ArabiTagMain.py 
	and a library XML file, load it into the database and then do all of the necessary cleaning
	executemany - zip
	"""
	altsplice = options.abt
	library = options.library	
	host = options.host
	database = options.database
	username = options.username
	password = options.password

	AddLibrary_ESTsToDb.main(altsplice, library, host, database, username, password)
	
def UpdateGenBank(option):
	"""
	Takes the output from ArabiTag_TableBuilding.py (An XML File) and load it into the database.
	
	Then, it uses the
	"""
	ans = ""
	while ans not in ['yes','no']:
		ans = raw_input("You are about to delete all GenBank information. The database will be inaccurate until you add new information to the database with the -a option. Are you sure you want to continue? (yes or no): ")
		if ans == "yes":
			db = MySQLdb.connect(host=host,user=username,passwd=password,db=database)
			db.autocommit(True)
			cursor = db.cursor()
			cursor.execute('DELETE FROM Library WHERE GenBank = True')
			cursor.close()
		else:
			print "Get your stuff straight, then you can come back."
			sys.exit(1)

def argParse():
	"""Parses all of the arguments and return the options from OptionParser"""
	use = """This is the ultimate database creation script. You will use this to create or replace a database, add libraries to a database, or update genbank information in the database. For more help, see the ArabiTag userguide at www.transvar.org/arabitag"""
	
	parser = OptionParser(usage=use)
	
	date = str(time.localtime().tm_mon)+"_"+str(time.localtime().tm_mday)+"_"+str(time.localtime().tm_year)
	
	parser.add_option("-c", "--create", dest="create", help="Creates a new database Note: will delete all existing ArabiTag tables from the database.", action="store_true")
	
	parser.add_option("-a","--add", dest="abt", help="Adds a .abt file that is accompanied with a library xml file to an existing database", default=None)
	
	parser.add_option("-b","--genbank", dest="genbank", help="Deletes existing GenBank libraries. Useful when adding an updated GenBank dbEST libraries.xml.", default=None)
		
	parser.add_option("-g","--genes", dest="genes", help="The genes in bed or psl format that the ESTs are mapped to", default=None)
		
	parser.add_option("-l","--library", dest="library", help="The library.xml file that is has the information about the ests one is adding")
	
	parser.add_option("-H", "--host", dest="host", help="[REQUIRED] The database host that ArabiTag will be communicating with", metavar="STRING", default=None)
	
	parser.add_option("-D", "--database", dest="database", help="[REQUIRED] The database to connect to on the host", default=None)
	
	parser.add_option("-U", "--username", dest="username", help="[REQUIRED] The username for the database specified with --database", metavar="STRING", default=None)
	
	parser.add_option("-P", "--password", dest="password", help="[REQUIRED] The password for the database specified with --database", metavar="STRING", default=None)
	
	(options, args) = parser.parse_args()
	return options

if __name__ == '__main__':

	options = argParse()#parse arguments
	
	if not (options.host or options.database or options.username or options.password):
		sys.stderr.write("ERROR!\n\tAll database information was not provided\n")
		exit(1)
	
	if options.create:
		if not (options.genes):
			sys.stderr.write("ERROR!\n\tTo create a database, you need to provide gene models\n")
			exit(1)
		print "Creating New Database"
		CreateNewDatabase(options)
	
	if options.genbank:
		print "Removing GenBank Information"
		UpdateGenBank(option)
		
	if options.abt:
		if not(options.library):
			sys.stderr.write("ERROR!\n\tIn Add to Database: Library.xml (-l) not specified\n")
			
		print "Adding To Database"
		AddToDatabase(options)
		
	
	
	
"""

cab

c - creates (or replaces) the database and populates the gene models within it - Gene Models are Required with this option
a - Adds .abt file (made from ArabiTagMain.py) into a database. The .abt file needs to be a run from a single library. A library can only be added once - if you want to add to/remove from a library, you must manually delete it and repeat the process of adding it. (use MySQL to get it out by library primary key (!!don't forget to remove the ESTs, too!!))
b - Removes the GenBank records of ests in the database and then reloads them using a Dump Provided by the Loraine Lab. If you want some of your libraries removed because you have since submitted them to GenBank

ArabiTag's Master Database Script!

Here's how you run this:

1) You have alignments XYZ.psl (it can also be a bed file) this is A SINGLE LIBRARY!!!
2) Run ArabiTagMain.py on your reference GeneModels.bed (or psl) and XYZ.psl (see ArabiTagMain.py) it become AltSplicing_XYZ.abt
3) Make a Library_XYZ.xml file (see BuildLibraryInfo.py)
4) Use some combination of these next three ways to run
A) If your database doesn't exist and you want to create one
	run this script with options  
		ArabiTagDB.py -c -g GeneModels.bed [then your database arguments (they're all required to login)]
	This [-c] creates (or replaces) the database on [-g GeneModels.bed] these gene models. 
	
	WARNING! -c will delete any existing databas and start from scratch. Use with CAUTION!
	
B) If your database exists and you want to add to it:
	run this script with options
		ArabiTagDB.py -a AltSplicing_XYZ.abt -l Library_XYZ.xml -e XYZ.psl -g GeneModels.bed [then your database arguments (they're all required to login)]
	--This [-a AltSplicing_XYZ.abt] adds the AltSplicing Support calculated by ArabiTagMain.py
		--Where
		The AltSplicing Support is from [-l Library_XYZ.xml] library XYZ
		The [-e XYZ.psl] ests are ALL (not just the ones that support AltSplicing) that are in the library
		The [-g GeneModels.bed] gene models that are supported by the ests
		
C) If you want to Update GenBank information (provided by transvar.org/arabitag in a GenBank_Version.asdump file)
	run this script with options
		ArabiTagDB.py -b GenBank_Version.asdump [then your database arguments (they're all required to login)]

	
NOTES ABOUT STEP 4:
	You can combine -c -a -b. For example:
		-c-a will create(or replace) a database AND add library specified by -a and it's options
		
		-c-b will create(or replace) a database AND update(or add) the GenBank data
		
		-a-b will add library specified by -a and its options AND update(or add) the GenBank data
		
		-c-a-b will create(or replace) a database AND add library specified by -a and it's options AND update(or add) the GenBank data

	



This is the only piece of code people will need to run ArabiTag.

The three main features
-----------------------
Creating a new Database
	Using ESTs and reference Gene Models and [Library Information]
	Run DbBuilding Script
	Library Information Builder
	Insert Lib Info
	Run ArabiTagMain.py
	Insert ArabiTag Info
	------------------------------------------------
	What we'll need
	------------------------------------------------
	DbBuilding Script - Done! src/DatabaseDevelopment/CreateDatabase.sql
	
	Library Information Builder - src/DatabaseDevelopment/BuildLibraryInfo.py
		Creates XML that has format
			<library name="required" desc="also required" genbank="True/False">
				<attribute label="label" id="#">Value</attribute>
				<attribute label="label" id="#">Value</attribute>
				<attribute label="label" id="#">Value</attribute>
				<members>
					<est>
						<name>This is the name that's in the alignment</name>
						<genbank_gi>something</genbank_gi>
						<genbank_acc>something</genbank_acc>
						<lastUpdate>current date</lastUpdate>
						<sequence>ATCATACATAGATAGATTATACGATCCACATGGACCAGATATAGCACCATGACCGATGAG</sequence>
					</est>
				</members>
			</library>
			
		if attribute id = -1, we're creating a new attribute label.
		else we only need to make an insert into LibAttrValue
		if genbank == False: mark this in the db for deletion later
		*******Current problems*******
			gi and acc aren't necessilary avaiable -- how do we handle this?
				GenBank will have it and the acc will be the est's name
		
	Loading Libraries.xml into database - I'm combining this with Library Information Builder
		we'll use mysqldb and a custom xml reader
		-need file, database and password
		simple library parse and inserts
		***this creates the libraries and the ests***
	
	ArabiTagMain creates AS and support information. The abt file has all the information we need.
		**Link the name to the id (This is going to create problems with LOTS of ESTs. -- make name a primary key?)
	
	Run script to create gaCount gpCount - I have thi somewhere


Adding Library of ESTs to Existing Database
	Same as before except no need to run dbcreation script so..
	..
	Library Information Builder
	Insert Lib Info
	Run ArabiTagMain.py
	Insert ArabiTag Info

Updating GenBank Records 
	Remove all non-user Libraries (genbank=TF column) Allow user to say if they want some/all of their libraries deleted (in case they put it in genbank and don't want redundant data.)
	
	Insert MySQL dump that we distribute bi-anually.
		MySQL dump building (We'll need this, not the user)
			Download dbEST
			Parse out Arabidopsis ***for other organisms, we should be able to specify this***
			Extract ESTs and Library Info
			Map ESTs
			Library Information Builder
			Create a new Database (see above)
			Dump the database
------------------------------------------------
"""
