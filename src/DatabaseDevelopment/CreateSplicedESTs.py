"""
Created by:
Wei Song, wsong4@uncc.edu
Archana Natarajan Raja, anatara5@uncc.edu
"""


"""BINF 6111/ITCS 8112 Project One."""


"""
Write a program that will read a set of EST/cDNA-to-genome alignments
in PSL format and emit only those lines that conform to the spliced EST
data set specification published on the UCSC Genome Bioinformatics Web
site.

Then, use the program to generate an intronEST data set for the
Arabidopsis thaliana genome.

You will not need to generate the alignments; these will be available
for download from:

http://teaching.transvar.org/data/LL_EST.sm.psl.gz

Genomic sequence for all five Arabidopsis nuclear chromosomes and
two organellar chromosomes are available at:

http:// teaching.transvar.org/data/chr1.fas.gz
http:// teaching.transvar.org/data/chr2.fas.gz
http:// teaching.transvar.org/data/chr3.fas.gz
http:// teaching.transvar.org/data/chr4.fas.gz
http:// teaching.transvar.org/data/chr5.fas.gz
http:// teaching.transvar.org/data/chrC.fas.gz [chloroplast]
http:// teaching.transvar.org/data/chrM.fas.gz [mitochondrial]

Information on psl format:
  http://genome.ucsc.edu/FAQ/FAQformat

spliced ESTs specification:
  http://genome.ucsc.edu/cgi-bin/hgTrackUi?g=intronEst
"""

import getopt,sys,os,gzip

def usage():
    """Print a helpful message regarding usage of this program to stderr."""
    txt = "Usage: python proj1.py [-i|--infile filename] [-o|--outfile filename]\n\nAssumes that files named for targets are in the current working directory.\nFor example, if the input file reports target foo, then there should be ONE\nfile named foo.fa* in the current working directory, where this script\nis invoked. If --infile is not supplied, obtains data from stdin.\nIf --outfile not supplied, writes to stdout.\n"
    sys.stderr.write(txt)

def getFileNames(args):
    """Retrieve input and output file names as two-item tuple.
    Returns None in place of input or output file name when not
    specified in args. Exits with an error if illegal options
    given."""
#    args = args.split()
    outfile = None
    infile = None
    opts,args = getopt.gnu_getopt(args,"ho:i:",["help","outfile=","infile="])

    for opt, arg in opts:
        if opt in ("-h","--help"):
            usage()
            sys.exit()
        elif opt in ("-i","--infile"):
            infile = arg
        elif opt in ("-o","--outfile"):
            outfile = arg
        else:
            raise getopt.GetoptError("Illegal option: " + opt)
    return (infile,outfile)


def readfile(fn=None):
    """
    Function: Open a file for reading. 
    Returns : a file object
    Args    : fn - the name of the file

    It can be a plain text file or a
    file compressed using gzip. If the latter,
    the file name must terminate in characters
    .gz
    """
    import gzip                   
    if not fn:
        sys.stderr.write("Error: need an input file\n")
        return None
    if fn.endswith('.gz'):
        fh=gzip.GzipFile(fn)
    else:
        fh=open(fn,'r')
    return fh

def findintron(start=None,end=None,sequence=None):
    """
    Function: to judge if the intron in query satisfied the spliced EST criteria: length >= 32 and both ends GT/AG or CT/AC
    Return: if satisfied the conditions, return True, else return False
    Args: the strand of the mapping: +/-, the start and end position of the intron in chromosome sequence, and the entire genomic sequence.
    """
    
    length = end - start +1  ## the length of the intron
    if (length >= 32):

        ## the nucleotides of two ends could be CT/AC
        """if (sequence[start] + sequence[start+1] + sequence[end-1] + sequence[end] == 'CTAC'):

            return True
     ## the nucleotides of two ends could be GT/AG
        elif (sequence[start] + sequence[start+1] + sequence[end-1] + sequence[end] == 'GTAG'):"""
                                                        
        return True                 
    return False


def judgeline(line=None,seq=None):
    """
    Function: reads all available lines from the given PSL line string,
              pick some of the data from each line, using spliced EST criteria to find the spliced EST lines.
    Returns : if the line is spliced EST, return True, else, return False.
    Args    : the line string and the sequence of relative chromosome 
    """
    e = {}
    d = {}
    m=0

    text = line.rstrip()
    fields = text.split('\t')
    if len(fields) != 21:  ## make sure it is PSL format
        sys.stderr.write("Error: not PSL format line")
        return
    else:  ## find several columns of the data for calculation, including strands, blocks and block starting positions.
        strand = fields[8]
        accession = fields[13]
        num_block = int(fields[17])
        tstart = int(fields[15])
        tend = int(fields[16])
        block_size = fields[18].split(',')
        block_size.remove(block_size[-1])
        qstarts = fields[19].split(',')
        qstarts.remove(qstarts[-1])
        tstarts = fields[20].split(',')
        tstarts.remove(tstarts[-1])
    if num_block > 1:   ## Splice EST requires at least one intron, so one exon situation should be excluded.
        #sequence=seq
        
        judge = False    
        for i in range(num_block-1):
            intron_start = int(tstarts[i]) + int(block_size[i])
            intron_end = int(tstarts[i+1])-1
                
            #judge = findintron(intron_start,intron_end,sequence)
            #judge = findintron(intron_start,intron_end)
            if intron_end - intron_start >= 32:#judge:  ## looking for at least one satisfied intron
                return True
    return False

def pslfile2output(infh,outfh):
    """
    Function: read each line from the input file, generate the related chromosome sequence and write the spliced EST lines to output file. 
    Return: write the spliced EST to output file
    Args: the input and output file objects
    """
    names = os.listdir('.')  ### find all the file names in current directory
    e = {}
    while 1:
        line = infh.readline()
        if not line:
            infh.close()
            break
        text = line.rstrip()
        fields = text.split('\t')
        if len(fields) != 21:    #we don't want the program to crash simply due to pragmas or comments
            if line.startswith("psL"):
                outfh.write(line)  
            continue
        accession = fields[13]

        """if not e.has_key(accession):
                    
            found = False
            ### looking for the fasta file in different format and extension.
            exts = ['.fas.gz', '.fas','.fa','.fasta','.fa.gz','.fasta.gz']
            for ext in exts:
                if not found:
                    file = accession + ext
                    for name in names:     ### check if the fasta file exists
                        if name == file:
                            found = True
                            e[accession] = getSequence(name)
                            break
            if not found:
                sys.stderr.write("Error:need input FASTA file:" + accession+'.fas')
                return

        seq = e[accession]"""

        #if judgeline(line,seq): ## if spliced EST, and canonical, write to the output file
        if judgeline(line):##Finding all spliced ESTs
            outfh.write(line)
 


def getSequence(fn=None):
    """
    Function: generate the nucleotides sequence from the give file
    Returns : the nucleotides sequence
    Args    : the name of the given file
    """
    line2=''
    k =1
    fl = readfile(fn)
    while 2:
      line1 = fl.readline()

      if not line1:
          fl.close()
          break

      if k > 1:  ## the sequence starts from second line
          line2 = line2 + line1.rstrip() ## combine each line of sequence to form the entire sequence
      k = k+1

    return line2

def checkinputfile(input = None):
    filenames = os.listdir('.')
    if input in filenames:

        return True
    else:
        return False

## You are welcome to modify this method, so long as the
## method of invoking the program stays the same. See the
## usage method for details.
##
## Note that your program will need to consult the target (chromosomal)
## sequences in order to filter the PSL lines. Assume that your program
## will be invoked in a directory containing fasta format files named for
## the target sequences. For instance, if a PSL file references a target
## named chr1, then your program should look for a file named chr1.fa*
## (e.g., chr1.fa, chr1.fasta, chr1.fas, etc.) and then use it to retrieve
## sequence for chr1. However, fasta format may not be the most efficient
## scheme for storing and accessing data on disk. Feel free to experiment
## with more efficient methods. However, if you do this, you will need to
## include some logic in your program that will create files in the new
## format and then delete them following execution. Also, feel free to
## add new options that modify the running of your program. For example,
## you may wish to implement a --debug flag, which might cause your
## program to print information about its state, errors, etc.


# For information on syntax of command line options, see:
# http://www.faqs.org/docs/diveintopython/kgp_commandline.html
# http://docs.python.org/library/getopt.html
if __name__ == "__main__":
    try:
#        (infile,outfile) = getFileNames(' '.join(sys.argv[1:]))
        (infile,outfile) = getFileNames(sys.argv[1:])
    except getopt.GetoptError:
        usage()
        # note the error code - 2
        sys.exit(2)

    check = checkinputfile(infile)
#    print check
    if not check:
        print "No such input file: " + infile
            
    if infile:
        if infile.endswith('.gz'):
            infh=gzip.GzipFile(infile)
        else:
            infh=open(infile,'r')
    else:
        infh = sys.stdin

            

    if outfile:
        outfh = open(outfile,'w')
    else:
        outfh = sys.stdout
    
    ## read the data from infh, write to
    ## outfh, then close the files if they are
    ## not stdin or stdout
    pslfile2output(infh,outfh) ## call the function and find the spliecd EST

    if infile:
        infh.close()
    if outfile:
        outfh.close()
    
            


