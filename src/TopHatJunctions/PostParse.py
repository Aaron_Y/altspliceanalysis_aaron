#!/usr/bin/env python
"""

Given a .abt file (from ArabiTagMain.py) reformat it into the format

chromosome	strand	start	end	type	Ga	Gp	libraryNameGacount	libraryNameGpCount

.abt format:

AGI Locus id
 Genome Model id for GA(intron)
 Genome Model id for GP(exon)
 ioverlap: start,length,strand
 eoverlap: start,length,strand
 ialt: start,length,strand or NA
 ioverlap ESTs: comma-separated list of accessions
 ialt ESTs: comma-separated list of accessions, or NA [non-RI only]
 eoverlap ESTs: comma-separated list of accessions, or NA [RI only]
 
args are [1] - infile [2] - outfile
"""

import sys,re,zipfile

from optparse import OptionParser

def getSpliceTypeAndDifferenceCoords(typ,iOverlap,eOverlap):
	"""
	returns the differance region of an alternative splicing event and what type
	of alternative splicing the event is.
	
	Types of Alternative Splicing -All Examples are + strand-
	Acceptor Site   XX---XXXX
                    XX----XXX
	Donor Site      XXX-----XX
                    XXXXX---XX

    Shift           XXXXX----XXXXX
                    XXX----XXXXXXX

    Retained Intron XXXX----XXX
                    XXXXXXXXXXX

    Exon Skipping   ----XXX----
                    -----------
	This method returns
	-What type of AS event this is, Difference in: Acceptor Site, Donor Site, Shift, Retained Intron, Exon Skipping
	-The coordinates of the AS event as a string "Start,End"
	"""
	(iStart, iLength, iStrand) = iOverlap.split(',')
	iStart = int(iStart)
	iLength = int(iLength)
	iEnd = iStart + iLength
	
	(eStart, eLength, eStrand) = eOverlap.split(',')
	eStart = int(eStart)
	eLength = int(eLength)
	eEnd = eStart + eLength

	drStart = max(iStart,eStart)
	drEnd = min(iEnd,eEnd)
	
	# no longer needed ? 
	#if typ == 'DS' and iStrand == -1:
	#	typ = 'AS'
	#elif typ == 'AS' and iStrand == -1:
	#	typ = 'DS'
	
	if typ in ['AS','DS']:
		if drStart == eStart and drEnd == eEnd:
			typ = typ+'/ES'
	diff = str(max(iStart,eStart))+','+str(min(iEnd,eEnd))

	return (typ,diff)

def getLocusToChr(bed):
	d = {}
	fh = readfile(bed)
	while 1:
		line = fh.readline()
		if not line:
			fh.close()
			break
		if line.startswith('#') or line.startswith('track'):
			continue
		toks = line.split('\t')
		name = toks[3]
		seqname = toks[0]
		# sanity checking
		if d.has_key(name):
			if d[name]!=name:
				raise ValueError("%s inconsistent chrom for %s"%(bed,name))
		d[name]=seqname
	return d

def readfile(f):
    """
    Function: Open a file for reading.
    Returns : a file handle
    Args    : the name of a file

    The given file can be compressed (file extension .gz)
    or a regular non-compressed file.

    """
    reg = re.compile(r'\.gz$')
    reg2 = re.compile(r'\.zip$')
    if reg.search(f):
        z = gzip.GzipFile(f)
    elif zipfile.is_zipfile(f):
        return None
    else:
        z = open(f)
    return z


if __name__ == '__main__':
	usage = "usage: %prog [options] in.txt out.txt"
	parser = OptionParser(usage)
	parser.add_option("-b","--bed_file",dest="bed",help="Annotations file in bed format. Needed for looking up chromosome [required]",
			  metavar="STRING",default=None)
	(options,args)=parser.parse_args()
	bed=options.bed
	if not bed:
		parser.print_help()
	locus2chr = getLocusToChr(bed)
	if not len(args) == 2:
		parser.print_help()
	(infile, outfile) = args
	fh = readfile(infile)
	outlines = {}# line upto ga/gp: {library:(gac,gap), library:(gac,gap)}
	libnames = {} #libraryName:0 --we'll use this to get the formatting of the outfile correct
	for line in fh.readlines():
		"""
	    The goal of this method is to change the structure of a line from the AltSplicing.abt
	
	    chr strand diffRegionGenomicStart diffRegionGenomicEnd type geneAbsent  genePresent libGa libGp 
		"""
		(locus,Ga,Gp,iOverlap,eOverlap,iAlt,typ,GaESTs,GpESTs) = line.split('\t')
		Ga_chr = locus2chr[Ga]
		Gp_chr = locus2chr[Gp]
		if not Ga_chr == Gp_chr:
			raise ValueError("Chromosome for Ga %s and Gp %s are different."%(Ga,Gp))
		curChr=Ga_chr
		GpESTs = GpESTs.strip()#remove \n
		(spliceType,diffCoords) = getSpliceTypeAndDifferenceCoords(typ,iOverlap,eOverlap)
		
		GaESTs = GaESTs.split(',')
		GaESTs.sort()
		GpESTs = GpESTs.split(',')
		GpESTs.sort()
		
		curLibs = {}
		
		for curEST in GaESTs:
			if curEST == '':
				continue
			(curName,curCount) = curEST.split('^')
			libnames[curName] = 0
			curLibs[curName] = (curCount,"0")
		
		for curEST in GpESTs:
			if curEST == '':
				continue
			(curName, curCount) = curEST.split('^')
			libnames[curName] = 0
			if curName in curLibs.keys():
				curLibs[curName] = (curLibs[curName][0], curCount)
			else:
				curLibs[curName] = ("0",curCount)
		#curChr = "chr" + re.match("AT(.)G",locus).groups()[0] 
		# this broke with rice
		strand = iOverlap.split(',')[-1]
		#chromosome	strand	start	end	type	Ga	Gp	
		(start,end) = diffCoords.split(',')
		outline = "\t".join([curChr,strand,start,end,spliceType,Ga,Gp])
		outlines[outline] = curLibs
	
	fh.close()
	fout = open(outfile,'w')
	#write header
	fout.write("chromosome\tstrand\tstart\tend\ttype\tGa\tGp")
	libnames = libnames.keys()
	libnames.sort()
	for name in libnames:
		# why do we need "sm" ?
		# maybe for add RI support step?
		# this ugly - changing it
		#fout.write('\tGa_%s.sm\tGp_%s.sm' % (name, name))
		# instead, should add ignoring multi-mapping reads
		# to upstream steps
		# this should be an option
		# as an option to upstream steps
		fout.write('\tGa_%s\tGp_%s' % (name, name))
	fout.write('\n')
	for key in outlines:
		fout.write(key)
		libs = outlines[key]
		for i in libnames:
			try:
				(GaCount,GpCount) = libs[i]
			except KeyError:
				GaCount = "0"
				GpCount = "0"
			fout.write('\t%s\t%s' %(GaCount, GpCount))
		fout.write('\n')
	fout.close()
	 
