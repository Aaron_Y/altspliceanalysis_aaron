"""

Use SamTools to get Sam lines for alignments that overlap RI difference regions and have 75M CIGAR code (perfect score)

Make a "Fake" Bedline from these groups of SAM lines with start = minStart and end= maxEnd and score = numreads within difference region of bed line.

Suggested path from this script: Combine these new "Fake" bedlines with TopHat's junctions.bed and then go through the ArabiTag/v3.0/src/TopHatJunctions pipeline (see README in that directory)

Notes:

	SamTools MUST be installed to run this script

FAQ: 
	Where do I get the RI difference Regions - It is the CSV output from ArabiTagOnline (Only select [x]RI as the AS type.
"""

from optparse import OptionParser
import sys, os, commands

def parseBAM_RI_Support(fn, diffRegions):
	"""
	Function: Extracts all BAM lines from fn that have 75M CIGAR code and are within diffRegions, then combines them to make a single bed line
	Args: fn - BAM filename  
		diffRegions - list of all the diffRegions we want ESTs within (returned from getAllDiffRegions)
	Returns: a list of bed lines
	"""	
	ret = []
	
	for region in diffRegions:
		overlaps = []
		samLines = commands.getoutput("samtools view %s %s" % (fn, region)).split('\n')
		for line in samLines:
			if line == "[bam_parse_region] invalid region."
				continue
			if line.split('\t')[5] == '75M':
				overlaps.append(line)
		
		ret.append(reduceMultipleSAMtoBed(overlaps))
		
	return ret
		
def reduceMultipleSAMtoBED(samLines):
	"""
	Function: Turns a list of SAM lines to a single bed line with start = minStart and end= maxEnd and score = number of SamLines
	Args: samLines: a list with structure [samDR1,samDR1,...]
	Returns: A Single bed line of Bed Lines
	
	NOTE:
	
	The Bedline will automatically be named after the last samLine we use
	start = minStart and end= maxEnd and score = number of SamLines
	
	e.x.  (S = Sam input, B = Bed output)
	
		SSSS SSS    SSS SSSSSS  SSS
		       SSS  S    SSS
		BBBBBBBBBBBBBBBBBBBBBBBBBBB
		
	SAM
	samFields = samLine.strip().split('\t')
		samFlag = int(samFields[1])
		# Only create a BED entry if the read was aligned
		if (samFlag & 0x0004):
			continue
		chrom = samFields[2]
		start = int(samFields[3])-1
		length = len(samFields[9])
		end = start + length
		name = samFields[0]	
		strand = getStrand(samFlag)
	
	"""
	minStart = None
	maxEnd = None 
	for line in samLines:
		samFields = line.strip().split('\t')
		samFlag = int(samFields[1])
		# Only create a BED entry if the read was aligned
		if (samFlag & 0x0004):
			continue
		chrom = samFields[2]
		start = int(samFields[3])-1
		length = len(samFields[9])
		end = start + length
		name = samFields[0]	
		strand = getStrand(samFlag)
		
		if minStart == None:
			minStart = start
			maxEnd = start+length
		else:
			if start < minStart:
				minStart = start
			if end > maxEnd:
				maxEnd = end
	return "%s\t%i\t%i\t%s\t%s\t%s\t%i\t%i\t%i\t%i\t%i,\t%i," % (chrom,start,end,name,len(samLines),strand,start,start,0,1,length,0)



def getStrand(samFlag):
	"""Get a string representation of the strand from the samFlag"""
	strand = "+"
	if (samFlag & (0x10)):	# minus strand if true.
		strand = "-"		
	return strand
	
def getAllDiffRegions(fn, FLANK=20):
	"""
	Function: Creates a list of all the difference regions in fn in the format ["chr_:start-stop",...]
	Args: fn - Difference 
		  FLANK - the amount of space we need each EST to strech  within the difference region
	Returns: The list of difference regions where each Dr is a single string "chr_:start-stop"
	
	The Flank Argument:
		Used when we want the ESTs we'll parse out in parseSAM to cover at least FLANK number of basepairs of the difference region.
		e.x.   (X = 10bp and FLANK is 20bp)
		
			XXXXXXXXXXXX     <--diffRegion 
			XX   XX   XXXX 	 <--Supports
		XXXXX    X        	 <--Doesn't Support
		
	fn is expected to be a .csv and have the format:
	as_id,gene_absent,gene_absent_ests,gene_present,gene_present_ests,chr,gdiff_start,gdiff_end,as_type,p
	
	0	  1			  2				   3			4				  5    6          7         8        9

	
	"""
	fh = open(fn,'r')
	fh.readline()#gets rid of header!! There Better be a Header if you got it from ArabiTagOnline
	
	ret = []
	
	for line in fh.readlines():
		dat = line.split(',')
		chrom = dat[5]
		start = int(dat[6])+FLANK
		end = int(dat[7])-FLANK
		ret.append("%s:%i-%i" % (chrom, start, end))
		
	return ret
		

if __name__ == '__main__':
	(bam, ri) = sys.argv[1:]
	
	diffs = getAllDiffRegions(ri)
	
	bedLines = parseBAM_RI_Support(bam, diffs)
	
	sys.stdout.write("\n".join(bedLines))
